//
//  DetailViewController.m
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Details";
    [self configureDetailPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureDetailPage {
    
    self.productImageView.image = self.productImage;
    self.productNameLabel.text = [self.product getEncodedProductName];
    self.longDescriptionLabel.attributedText = [self.product getAttributedProductLongDescription];
    self.priceLabel.text = self.product.price;
    self.inStockLabel.text = self.product.inStock == YES ? @"In Stock" : @"Not in stock";
    self.starCountLabel.text = self.product.reviewRating.stringValue;
    self.reviewsCountLabel.text = [NSString stringWithFormat:@"%ld reviews", self.product.reviewCount.integerValue];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
