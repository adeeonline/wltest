//
//  BaseModel.h
//  
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//


#import <Mantle/Mantle.h>
#import "APIClient.h"

@interface BaseModel : MTLModel <MTLJSONSerializing>

+ (NSDictionary *)mergeJSONKeyPathsWithKeyPathDictionary:(NSDictionary *)dict;
+ (instancetype)convertResponseFromJSONDictionary:(NSDictionary *)JSONDictionary toModelClass:(Class)modelClass;

+ (Class)modelClass;
+ (NSString *)endpoint;
+ (NSString *)responseObjectKey;

+ (NSString *)getFormattedResourceName:(NSString *)className;

#pragma mark - API methods

+ (void)getList:(NSDictionary *)filters withSuccessHandler:(successHandler)successHandler andFailureHanlder:(failureHandler)failureHandler;

@end
