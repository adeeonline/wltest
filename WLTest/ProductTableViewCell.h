//
//  ProductTableViewCell.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productName;
@property (weak, nonatomic) IBOutlet UILabel *productShortDescription;

@end
