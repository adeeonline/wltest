//
//  APIClient.m
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//
#import "APIClient.h"
#import "BaseModel.h"
#import "Meta.h"
#import <InflectorKit/NSString+InflectorKit.h>

@implementation APIClient

+ (instancetype)sharedClient {

    static APIClient *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *baseUrl = [NSURL URLWithString:BASE_URL];
        sharedClient = [[APIClient alloc] initWithBaseURL:baseUrl];
        sharedClient.requestSerializer = [AFJSONRequestSerializer serializer];
        sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
        sharedClient.generalCache = [[NSCache alloc] init];
    });

    return sharedClient;
}

+ (NSDictionary *)addMetaDataToFilters:(NSDictionary *)filters {
    
    NSMutableDictionary *dict = [filters mutableCopy];
    if (!filters) {
        dict = [NSMutableDictionary new];
    }
    
    return dict;
}

+ (NSString *)addMetaDataFromFilters:(NSDictionary *)filters ToEndpoint:(NSString *)endpoint {
    
    NSNumber *pageSize = [filters objectForKey:PAGE_SIZE_KEY];
    NSNumber *page = [filters objectForKey:PAGE_NUMBER_KEY];
    
    return [NSString stringWithFormat:@"%@/%@/%@/%@", endpoint, API_KEY, page.stringValue, pageSize.stringValue];
}

#pragma mark - REST API methods below

- (void)getListForResource:(NSString *)endpoint forModel:(Class)model withFilters:(NSDictionary *)filters successHandler:(successHandler)successHandler failureHanlder:(failureHandler)failureHandler {
    
    filters = [APIClient addMetaDataToFilters:filters];
    APIClient *client = [APIClient sharedClient];
    Class modelClass = model;
    
    endpoint = [APIClient addMetaDataFromFilters:filters ToEndpoint:endpoint];
    
    [client GET:endpoint parameters:filters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        id response = responseObject;
        if ([modelClass responseObjectKey]) {
             response = [responseObject objectForKey:[modelClass responseObjectKey]];
        }
        
        NSMutableArray *responseArray = [[NSMutableArray alloc] initWithCapacity:[responseObject count]];
        for (int i = 0; i < [response count]; i++)
        {
            id formattedObject = [BaseModel convertResponseFromJSONDictionary:response[i] toModelClass:modelClass];
            [responseArray addObject:formattedObject];
        }
        
        id meta = [responseObject mutableCopy];
        [meta removeObjectForKey:[modelClass responseObjectKey]];
        id metaModel = [BaseModel convertResponseFromJSONDictionary:meta toModelClass:[Meta class]];
            
        successHandler(task, responseArray, metaModel);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", [[error userInfo] objectForKey:AFURLResponseSerializationErrorDomain]);
        id finalErrorBody = nil;
        NSData *responseBody = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSError *jsonParseError;
        NSDictionary *errorBody = @{};
        
        if (responseBody != nil) {
            errorBody = [NSJSONSerialization JSONObjectWithData:responseBody options:0 error:&jsonParseError];
        }
        
        if (jsonParseError) {
            finalErrorBody = @{@"error": @"true", @"message": [NSString stringWithUTF8String:[responseBody bytes]]};
        } else {
            finalErrorBody = errorBody;
        }
        
        failureHandler(task, error, finalErrorBody);
    }];
}

@end
