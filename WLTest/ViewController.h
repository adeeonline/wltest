//
//  ViewController.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *productsTableView;
@property (nonatomic, strong) NSMutableArray *products;

@end

