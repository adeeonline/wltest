//
//  ViewController.m
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "ViewController.h"
#import "WalmartProduct.h"
#import "Meta.h"
#import "ProductTableViewCell.h"
#import "UIKit+AFNetworking.h"
#import "DetailViewController.h"

@interface ViewController ()

@property (nonatomic, strong) WalmartProduct *selectedProduct;
@property (nonatomic, strong) UIImage *selectedProductImage;

@property (nonatomic, strong) NSNumber *pageNumber;
@property (nonatomic, strong) NSNumber *defaultPageSize;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Walmart Labs";
    
    self.productsTableView.rowHeight = UITableViewAutomaticDimension;
    self.productsTableView.estimatedRowHeight = 180.0f;
    
    self.pageNumber = @1;
    self.defaultPageSize = [NSNumber numberWithInteger:[DEFAULT_REQUEST_PAGE_SIZE integerValue]];
    
    NSMutableDictionary *filters = [NSMutableDictionary new];
    [filters setObject:self.pageNumber forKey:PAGE_NUMBER_KEY];
    [filters setObject:self.defaultPageSize forKey:PAGE_SIZE_KEY];
    
    __weak ViewController *weakSelf = self;
    [WalmartProduct getList:filters withSuccessHandler:^(NSURLSessionDataTask *data, id responseObject, id meta) {
        weakSelf.products = [responseObject mutableCopy];
        [weakSelf.productsTableView reloadData];
    } andFailureHanlder:^(NSURLSessionDataTask *data, NSError *error, id responseObject) {
        NSLog(@"%@", responseObject);
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"productDetail"]) {
        DetailViewController *controller = segue.destinationViewController;
        controller.product = self.selectedProduct;
        controller.productImage = self.selectedProductImage;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Datasource / Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.products.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.products.count) {
        return [self getLoadingCell];
    }
    
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [ProductTableViewCell new];
    }
    
    WalmartProduct *product = [self.products objectAtIndex:indexPath.row];
    
    cell.imageView.image = nil;
    cell.productName.text = [product getEncodedProductName];
    cell.productShortDescription.attributedText = [product getAttributedProductDescription];
    
    [cell.productImage setImageWithURL:product.productImage placeholderImage:nil];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.selectedProduct = [self.products[indexPath.row] copy];
    ProductTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self.selectedProductImage = cell.productImage.image;
    
    [self.productsTableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"productDetail" sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell isKindOfClass:[ProductTableViewCell class]]) {
        return;
    }
    
    self.pageNumber = [NSNumber numberWithInteger:[self.pageNumber integerValue] + 1];
    
    NSMutableDictionary *filters = [NSMutableDictionary new];
    [filters setObject:self.pageNumber forKey:PAGE_NUMBER_KEY];
    [filters setObject:self.defaultPageSize forKey:PAGE_SIZE_KEY];
    
    __weak ViewController *weakSelf = self;
    [WalmartProduct getList:filters withSuccessHandler:^(NSURLSessionDataTask *data, id responseObject, id meta) {
        [weakSelf.products addObjectsFromArray:responseObject];
        [weakSelf.productsTableView reloadData];
    } andFailureHanlder:^(NSURLSessionDataTask *data, NSError *error, id responseObject) {
        NSLog(@"%@", responseObject);
    }];
}

- (UITableViewCell *)getLoadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGPoint p = CGPointMake(cell.center.x + activityIndicator.frame.size.width * 2, cell.center.y);
    activityIndicator.center = p;
    [cell addSubview:activityIndicator];
    cell.backgroundColor = [UIColor clearColor];
    [activityIndicator startAnimating];
    
    return cell;
}


@end
