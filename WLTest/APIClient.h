//
//  APIClient.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "APIConstants.h"

@interface APIClient : AFHTTPSessionManager

@property (nonatomic, strong) NSCache *generalCache;

+ (instancetype)sharedClient;
+ (NSDictionary *)addMetaDataToFilters:(NSDictionary *)filters;

#pragma mark - response handler blocks

typedef void (^successHandler)(NSURLSessionDataTask *data, id responseObject, id meta);
typedef void (^failureHandler)(NSURLSessionDataTask *data, NSError *error, id responseObject);

#pragma mark - REST API Methods

- (void)getListForResource:(NSString *)endpoint forModel:(Class)model withFilters:(NSDictionary *)filters successHandler:(successHandler)successHandler failureHanlder:(failureHandler)failureHandler;



@end
