//
//  ProductTableViewCell.m
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "ProductTableViewCell.h"

@implementation ProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
