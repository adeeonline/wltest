//
//  WalmartProduct.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "BaseModel.h"

@interface WalmartProduct : BaseModel

@property (nonatomic, copy, readonly) NSString *productId;
@property (nonatomic, copy, readonly) NSString *productName;
@property (nonatomic, copy, readonly) NSString *shortDescription;
@property (nonatomic, copy, readonly) NSString *longDescription;
@property (nonatomic, copy, readonly) NSString *price;
@property (nonatomic, copy, readonly) NSURL *productImage;
@property (nonatomic, copy, readonly) NSNumber *reviewRating;
@property (nonatomic, copy, readonly) NSNumber *reviewCount;
@property (nonatomic, readonly) BOOL inStock;

- (NSAttributedString *)getAttributedProductDescription;
- (NSAttributedString *)getAttributedProductLongDescription;
- (NSString *)getEncodedProductName;

@end
