//
//  DetailViewController.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WalmartProduct.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) WalmartProduct *product;
@property (nonatomic, strong) UIImage *productImage;

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *inStockLabel;
@property (weak, nonatomic) IBOutlet UILabel *longDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *starCountLabel;

@end
