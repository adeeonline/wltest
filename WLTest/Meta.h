//
//  Meta.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "BaseModel.h"

@interface Meta : BaseModel

@property (nonatomic, copy, readonly) NSString *etag;
@property (nonatomic, copy, readonly) NSString *requestId;
@property (nonatomic, copy, readonly) NSString *kind;
@property (nonatomic, copy, readonly) NSNumber *pageNumber;
@property (nonatomic, copy, readonly) NSNumber *pageSize;
@property (nonatomic, copy, readonly) NSNumber *status;
@property (nonatomic, copy, readonly) NSNumber *totalProducts;

@end
