//
//  WalmartProduct.m
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "WalmartProduct.h"

@implementation WalmartProduct

+ (NSString *)responseObjectKey {
    return @"products";
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"productId" : @"productId",
             @"productName" : @"productName",
             @"shortDescription" : @"shortDescription",
             @"longDescription" : @"longDescription",
             @"price" : @"price",
             @"productImage" : @"productImage",
             @"reviewRating" : @"reviewRating",
             @"reviewCount" : @"reviewCount",
             @"inStock" : @"inStock"
             };
}

+ (NSValueTransformer *)productImageJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSString *)endpoint {
    return @"walmartproducts";
}

- (NSAttributedString *)getAttributedProductDescription
{
    NSString *prodDesc = self.shortDescription != nil ? self.shortDescription : self.longDescription;
    
    NSData *data = [prodDesc dataUsingEncoding:NSUTF8StringEncoding];
    prodDesc = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    return [[NSAttributedString alloc] initWithData:[prodDesc dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
}

- (NSAttributedString *)getAttributedProductLongDescription
{
    NSString *prodDesc = self.longDescription != nil ? self.longDescription : self.shortDescription;
    
    NSData *data = [prodDesc dataUsingEncoding:NSUTF8StringEncoding];
    prodDesc = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    
    return [[NSAttributedString alloc] initWithData:[prodDesc dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
}

- (NSString *)getEncodedProductName {
    NSData *data = [self.productName dataUsingEncoding:NSUTF8StringEncoding];
    return [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
}


@end
