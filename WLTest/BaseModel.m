//
//  BaseModel.m
//
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "BaseModel.h"
#import "APIClient.h"
#import <DateTools/DateTools.h>
#import <InflectorKit/NSString+InflectorKit.h>

@implementation BaseModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{};
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    return self;
}

+ (NSDictionary *)mergeJSONKeyPathsWithKeyPathDictionary:(NSDictionary *)dict {
    
    return [dict mtl_dictionaryByAddingEntriesFromDictionary:[BaseModel JSONKeyPathsByPropertyKey]];
}

+ (instancetype)convertResponseFromJSONDictionary:(NSDictionary *)JSONDictionary toModelClass:(Class)modelClass {
    
    NSError *error;
    id obj = nil;
    if (modelClass != nil) {
        obj = [MTLJSONAdapter modelOfClass:modelClass fromJSONDictionary:JSONDictionary error:&error];
    }
    else {
        obj = [MTLJSONAdapter modelOfClass:[self modelClass] fromJSONDictionary:JSONDictionary error:&error];
    }
    
    
    if (error) {
        NSLog(@"%@", [error description]);
    }
    
    return obj;
}

+ (NSString *)endpoint {
    return nil;
}

+ (Class)modelClass {
    return [self class];
}

+ (NSString *)responseObjectKey {
    return nil;
}


#pragma mark - REST Helpers

+ (void)getList:(NSDictionary *)filters withSuccessHandler:(successHandler)successHandler andFailureHanlder:(failureHandler)failureHandler {

    NSString *resourceName = [BaseModel getFormattedResourceName:NSStringFromClass([self class])];
    resourceName = [resourceName pluralizedString];
    
    if ([self endpoint] != nil) {
        resourceName = [self endpoint];
    }
    
    APIClient *client = [APIClient sharedClient];
    [client getListForResource:resourceName forModel:[self modelClass] withFilters:filters successHandler:successHandler failureHanlder:failureHandler];
}

+ (NSString *)getFormattedResourceName:(NSString *)className
{
    NSMutableString *tempStr = [NSMutableString new];
    for (NSInteger i=0; i<className.length; i++)
    {
        NSString *ch = [className substringWithRange:NSMakeRange(i, 1)];
        if ([ch rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]].location != NSNotFound) {
            [tempStr appendString:@" "];
        }
        [tempStr appendString:ch];
    }
    
    tempStr = [[tempStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] mutableCopy];
    
    NSMutableArray *resourceComponents = [[tempStr componentsSeparatedByString:@" "] mutableCopy];
    for (NSInteger i=0; i<resourceComponents.count; i++) {
        resourceComponents[i] = [resourceComponents[i] lowercaseString];
    }
    resourceComponents[resourceComponents.count - 1] = [[[resourceComponents lastObject] pluralizedString] capitalizedString];
    
    return [resourceComponents componentsJoinedByString:@"-"];
}


@end
