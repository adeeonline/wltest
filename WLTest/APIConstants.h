//
//  APIConstants.h
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#define API_KEY @"b9782ffd-f2c4-4a96-8e92-43412fea4121"
#define BASE_URL @"https://walmartlabs-test.appspot.com/_ah/api/walmart/v1"

#define PAGE_SIZE_KEY @"pageSize"
#define PAGE_NUMBER_KEY @"pageNumber"

#define DEFAULT_REQUEST_PAGE_SIZE @"10"