//
//  Meta.m
//  WLTest
//
//  Created by Assad Ullah on 6/8/16.
//  Copyright © 2016 Assad Ullah. All rights reserved.
//

#import "Meta.h"

@implementation Meta

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"etag": @"etag",
             @"requestId": @"id",
             @"kind": @"kind",
             @"pageNumber": @"pageNumber",
             @"pageSize": @"pageSize",
             @"status": @"status",
             @"totalProducts": @"totalProducts"
            };
}

@end
